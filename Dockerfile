FROM python:3.9 AS build
WORKDIR /project
COPY ./app/requirements.txt /project
RUN pip install -r requirements.txt

FROM python:3.9 AS app
ENV PYTHONUNBUFFERED 1
WORKDIR /project
COPY --from=build /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
COPY ./app /project
